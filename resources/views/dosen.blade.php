@extends('layout.main')
@section('title','Data Dosen')

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Dosen</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width: 10px;">No</th>
                        <th>Nama</th>
                        <th>Pendidikan</th>
                        <th>Bidang Konsentrasi</th>
                        <th>Alamat</th>
                    </tr>
                </thead>
                <tbody>
                    @php(
                            $no=1
                        )
                    @foreach($dsn as $data)
                    <tr>
                        <td align="center">{{ $no++ }}</td>
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->pendidikan_terakhir }}</td>
                        <td>{{ $data->bidang }}</td>
                        <td>{{ $data->alamat }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
