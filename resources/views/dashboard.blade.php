@extends('layout.main')
@section('title','Data Mahasiswa')

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Dashboard</h6>
    </div>
        <h3 class="btn btn-danger m-4 p-4">Anda Login dengan Username : {{ Auth()->user()->username }}</h3>
        <h3 class="btn btn-success m-4 p-4">Level User Anda : {{ Auth()->user()->level }}</h3>
    </div>
    </div>
</div>
@endsection
