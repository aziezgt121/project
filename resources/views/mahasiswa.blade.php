@extends('layout.main')
@section('title','Data Mahasiswa')

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Mahasiswa</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width: 10px;">No</th>
                        <th style="width: 170px;">NPM</th>
                        <th>Nama</th>
                        <th>Fakultas</th>
                        <th>Prodi</th>
                    </tr>
                </thead>
                <tbody>
                    @php(
                            $no=1
                        )
                    @foreach($mhs as $data)
                    <tr>
                        <td align="center">{{ $no++ }}</td>
                        <td>{{ $data->npm }}</td>
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->fakultas }}</td>
                        <td>{{ $data->prodi }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
