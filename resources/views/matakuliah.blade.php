@extends('layout.main')
@section('title','Data Dosen')

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Matakuliah</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width: 10px;">No</th>
                        <th>Kode MK</th>
                        <th>Nama MK</th>
                        <th>SKS</th>
                        <th>Semester</th>
                    </tr>
                </thead>
                <tbody>
                    @php(
                            $no=1
                        )
                    @foreach($mk as $data)
                    <tr>
                        <td align="center">{{ $no++ }}</td>
                        <td>{{ $data->kode_mk }}</td>
                        <td>{{ $data->nama_mk }}</td>
                        <td>{{ $data->sks }}</td>
                        <td>{{ $data->semester }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
