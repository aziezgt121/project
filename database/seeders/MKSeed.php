<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\Model_matakuliah;
class MKSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
        	[
        		"kode_mk"=>"MK001",
        		"nama_mk"=>"Algoritma Struktur Data",
        		"sks"=>4,
        		"semester"=>"4",
        	],
        	[
        		"kode_mk"=>"MK002",
        		"nama_mk"=>"Basis Data",
        		"sks"=>2,
        		"semester"=>"4",
        	],
        	[
        		"kode_mk"=>"MK003",
        		"nama_mk"=>"Programming Web",
        		"sks"=>3,
        		"semester"=>"3",
        	],
        	[
        		"kode_mk"=>"MK004",
        		"nama_mk"=>"Keamanan Komputer Jaringan",
        		"sks"=>3,
        		"semester"=>"5",
        	],
        	[
        		"kode_mk"=>"MK005",
        		"nama_mk"=>"Programming Visual",
        		"sks"=>4,
        		"semester"=>"4",
        	],
        	[
        		"kode_mk"=>"MK006",
        		"nama_mk"=>"Pengolahan Citra",
        		"sks"=>4,
        		"semester"=>"4",
        	],

        ];
        foreach ($user as $key => $value) {
        	Model_matakuliah::create($value);
        }
    }
}
