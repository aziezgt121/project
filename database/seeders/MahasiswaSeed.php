<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\Model_mahasiswa;

class MahasiswaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
        	[
        		"npm"=>"153510374",
        		"nama"=>"Abdullah",
        		"fakultas"=>"Teknik",
        		"prodi"=>"Teknik Informatika",
        	],
        	[
        		"npm"=>"153510375",
        		"nama"=>"Budi Hamdani",
        		"fakultas"=>"Teknik",
        		"prodi"=>"Teknik Informatika",
        	],
        	[
        		"npm"=>"154110212",
        		"nama"=>"Ridwan",
        		"fakultas"=>"Pertanian",
        		"prodi"=>"Agroteknologi",
        	],
        	[
        		"npm"=>"153510124",
        		"nama"=>"Siti Maimunah",
        		"fakultas"=>"Teknik",
        		"prodi"=>"Teknik Informatika",
        	],
        	[
        		"npm"=>"153510376",
        		"nama"=>"Sulaiha",
        		"fakultas"=>"Teknik",
        		"prodi"=>"Teknik Informatika",
        	],
        	[
        		"npm"=>"153510124",
        		"nama"=>"Muhammad Zaid",
        		"fakultas"=>"Teknik",
        		"prodi"=>"Teknik Informatika",
        	],

        ];
        foreach ($user as $key => $value) {
        	Model_mahasiswa::create($value);
        }
    }
}
