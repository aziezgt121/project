<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
        	[
        		"name"=>"admin",
        		"username"=>"admin",
        		"email"=>"admin@gmail.com",
        		"password"=> bcrypt("123456"),
        		"level"=>"admin",
        	],
        	[
        		"name"=>"dosen",
        		"username"=>"dosen",
        		"email"=>"dosen@gmail.com",
        		"password"=> bcrypt("123456"),
        		"level"=>"dosen",
        	],
        	[
        		"name"=>"mahasiswa",
        		"username"=>"mahasiswa",
        		"email"=>"mahasiswa@gmail.com",
        		"password"=> bcrypt("123456"),
        		"level"=>"mahasiswa",
        	]
        ];
        foreach ($user as $key => $value) {
        	User::create($value);
        }
    }
}
