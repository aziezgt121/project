<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\Model_dosen;

class DosenSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
        	[
        		"nama"=>"Dr. Abdul Rahmad, MT",
        		"pendidikan_terakhir"=>"S3",
        		"bidang"=>"Programming",
        		"alamat"=>"Riau",
        	],
        	[
        		"nama"=>"Muhammad Ali, MT",
        		"pendidikan_terakhir"=>"S2",
        		"bidang"=>"Networking",
        		"alamat"=>"Riau",
        	],
        	[
        		"nama"=>"Arifin Bustami, MT",
        		"pendidikan_terakhir"=>"S2",
        		"bidang"=>"Artificial Intelegen",
        		"alamat"=>"Riau",
        	],
        	[
        		"nama"=>"Rahmat Hidayat, M.Kom",
        		"pendidikan_terakhir"=>"S2",
        		"bidang"=>"Programming",
        		"alamat"=>"Riau",
        	],
        	[
        		"nama"=>"Dr. Rudiansyah, M.Kom",
        		"pendidikan_terakhir"=>"S3",
        		"bidang"=>"Programming",
        		"alamat"=>"Riau",
        	],
        	[
        		"nama"=>"Dr. Lia Listiani, MT",
        		"pendidikan_terakhir"=>"S3",
        		"bidang"=>"Networking",
        		"alamat"=>"Riau",
        	],

        ];
        foreach ($user as $key => $value) {
        	Model_dosen::create($value);
        }
    }
}
