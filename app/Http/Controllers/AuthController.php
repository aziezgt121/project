<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
 	public function index()
 	{
 		return view('login');
 	}
 	public function dashboard()
 	{
 		return view('dashboard');
 	}
 	public function proses_login(Request $request)
 	{
 		request()->validate([
 				'username' => 'required',
 				'password' => 'required',
 			]);
	 		$kredensil = $request->only('username', 'password');

 			if(Auth::attempt($kredensil)){
 				$user = \Auth::user();
 				if($user){
 					return redirect()->intended('dashboard');
 				}
 				// if($user->level == 'admin'){
 				// 	return redirect()->intended('admin');
 				// }elseif($user->level == 'mahasiswa'){
 				// 	return redirect()->intended('mahasiswas');
 				// }elseif($user->level == 'dosen'){
 				// 	return redirect()->intended('dosen');
 				// }

 				return redirect()->intended('/');
 			}
 			
 			return redirect('/');
 	}
 	public function logout(Request $request)
 	{
 		$request->session()->flush();
 		Auth::logout();
 		return redirect('login');
 	}

}
