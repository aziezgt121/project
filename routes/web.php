<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\MatakuliahController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/login', 'App\Http\Controllers\AuthController@index')->name('login');
Route::post('/proses_login', 'App\Http\Controllers\AuthController@proses_login')->name('proses_login');
Route::get('/logout', 'App\Http\Controllers\AuthController@logout')->name('logout');
Route::get('/dashboard', 'App\Http\Controllers\AuthController@dashboard')->name('dashboard');
// 


Route::group(['middleware'=>['auth','cek_login:admin']], function(){

		Route::get('dosen', [DosenController::class, 'index'])->name('dosen');

});

Route::group(['middleware'=>['auth','cek_login:admin,dosen']], function(){

		Route::get('mahasiswa', [MahasiswaController::class, 'index'])->name('mahasiswa');

});

Route::group(['middleware'=>['auth','cek_login:admin,mahasiswa']], function(){

		Route::get('matakuliah', [MatakuliahController::class, 'index'])->name('matakuliah');

});

Route::group(['middleware'=>['auth','cek_login:admin,mahasiswa']], function(){

		Route::get('matakuliah', [MatakuliahController::class, 'index'])->name('matakuliah');

});

